package com.bitbucket.rpdmiranda.utils.rancher1to2.rancher16

data class DockerCompose(
        var version: String? = null,
        var services: Map<String, Docker16Service> = HashMap(),
        var volumes: Map<String, Docker16Volume> = HashMap()
)

data class Docker16Service(
        var cap_add: List<String> = ArrayList(),
        var image: String? = null,
        var user: String? = null,
        var environment: Any? = null,
        var env_file: List<String> = ArrayList(),
        var volumes: List<String> = ArrayList(),
        var network_mode: String? = null,
        var stdin_open: Boolean? = null,
        var tty: Boolean? = null,
        var labels: Map<String, String> = HashMap(),
        var links: List<String> = ArrayList(),
        var logging: Docker16Logging? = null,
        var ulimits: Docker16Ulimits? = null,
        var command: Any? = null,
        var ports: Any? = null,
        var restart: String? = null,
        var mem_limit: Long? = null,
        var mem_reservation: Long? = null,
        var cpu_shares: Long? = null,
        var depends_on: List<String> = ArrayList(),
        var container_name: String? = null,
        var hostname: String? = null,
        var entrypoint: String? = null,
        var working_dir: String? = null,
        var expose: List<String> = ArrayList()
)

data class Docker16Ulimits(
        var memlock: Docker16Memlock? = null
)

data class Docker16Memlock(
        var soft: Long? = null,
        var hard: Long? = null
)

data class Docker16Logging(
        var options: Map<String, String> = HashMap()
)

data class Docker16Volume(
        var driver_opts: Map<String, String> = HashMap()
)
