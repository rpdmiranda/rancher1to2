package com.bitbucket.rpdmiranda.utils.rancher1to2

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@ConfigurationProperties("rancher1to2")
@Component
data class Configuracao(

        var dir: String? = null,

        var registry: String? = null
)
