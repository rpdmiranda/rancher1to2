package com.bitbucket.rpdmiranda.utils.rancher1to2

import com.bitbucket.rpdmiranda.utils.rancher1to2.rancher16.DockerCompose
import com.bitbucket.rpdmiranda.utils.rancher1to2.rancher16.RancherCompose
import com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import io.kubernetes.client.models.*
import org.springframework.stereotype.Component
import org.yaml.snakeyaml.Yaml
import java.io.File


@Component
class ProcessadorPilha(
        val configuracao: Configuracao,
        val processadorPod: ProcessadorPod
) {

    val logger = logger()

    /**
     * Processa os arquivos de uma pilha
     */
    fun processarPilha(diretorio: File) {
        val dockerFile = diretorio.resolve("docker-compose.yml")
        val rancherFile = diretorio.resolve("rancher-compose.yml")

        assert(dockerFile.isFile && dockerFile.canRead()) { "Não consegui ler $dockerFile" }
        assert(rancherFile.isFile && rancherFile.canRead()) { "Não consegui ler $rancherFile" }

        val nomePilha = diretorio.name

        logger.info("Processando pilha {}", nomePilha)

        val yaml = Yaml()
        val docker = dockerFile.reader().use { yaml.loadAs(it, DockerCompose::class.java) }
        val rancher = rancherFile.reader().use { yaml.loadAs(it, RancherCompose::class.java) }

        val namespace = V1NamespaceBuilder()
                .withApiVersion("v1")
                .withKind("Namespace")
                .withNewMetadata().withName(nomePilha).endMetadata()
                .build()

        val daemonSets = arrayListOf<V1beta1DaemonSet>()
        val deployments = arrayListOf<V1Deployment>()
        docker.services.forEach {

            val service = it.value
            val serviceName = it.key

            val serviceMetadata = V1ObjectMetaBuilder()
                    .withName(serviceName)
                    .withNamespace(nomePilha)
                    .withLabels(mapOf(Pair("app.kubernetes.io/name", serviceName)))
                    .build()

            val podTemplate = processadorPod.buildPodTemplate(service, serviceName, docker.volumes, serviceMetadata)

            val serviceLabels = service.labels
            val schedulerGlobal = serviceLabels
                    .getOrDefault("io.rancher.scheduler.global", "false")
                    .toBoolean()

            if (schedulerGlobal) {
                daemonSets.add(V1beta1DaemonSetBuilder()
                        .withApiVersion("extensions/v1beta1")
                        .withKind("DaemonSet")
                        .withMetadata(serviceMetadata)

                        .withNewSpec()
                        .withNewSelector().withMatchLabels(mapOf("app.kubernetes.io/name" to serviceName)).endSelector()
                        .withTemplate(podTemplate)
                        .endSpec()
                        .build())

                val mapper = ObjectMapper(YAMLFactory())
                mapper.setSerializationInclusion(NON_NULL);
                mapper.writeValue(System.out, daemonSets)
                println("---")
            }
        }
    }

}
