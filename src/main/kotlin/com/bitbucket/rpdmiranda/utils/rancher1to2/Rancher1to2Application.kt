package com.bitbucket.rpdmiranda.utils.rancher1to2

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import java.io.File
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    runApplication<Rancher1to2Application>(*args)
}

@SpringBootApplication
@EnableConfigurationProperties
class Rancher1to2Application(val processadorPilha: ProcessadorPilha, val configuracao: Configuracao) : CommandLineRunner {

    val logger = logger()

    override fun run(vararg args: String?) {
        if (configuracao.dir == null) {
            logger.error("Especifique o parâmetro rancher1to2.dir com o diretório")
            exitProcess(1);
        }

        val dir = File(configuracao.dir);
        if (!dir.isDirectory || !dir.canRead()) {
            logger.error("Não consegui ler o diretório {}", dir)
            exitProcess(1);
        }
        logger.info("Procurando pilhas em {}", dir)

        recuperarPilhas(dir).forEach { processadorPilha.processarPilha(it) }
    }

    private fun recuperarPilhas(dir: File): Sequence<File> {
        return dir.walk().filter {
            if (!it.isDirectory)
                return@filter false
            val rancherCompose = it.resolve("rancher-compose.yml")
            val dockerCompose = it.resolve("docker-compose.yml")

            // Se os arquivos -compose existem e podem ser lidos
            rancherCompose.canRead() && rancherCompose.isFile &&
                    dockerCompose.canRead() && dockerCompose.isFile
        }
    }
}

