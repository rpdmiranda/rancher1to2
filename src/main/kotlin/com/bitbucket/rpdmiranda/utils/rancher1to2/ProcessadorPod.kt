package com.bitbucket.rpdmiranda.utils.rancher1to2

import com.bitbucket.rpdmiranda.utils.rancher1to2.rancher16.Docker16Service
import com.bitbucket.rpdmiranda.utils.rancher1to2.rancher16.Docker16Volume
import io.kubernetes.client.models.*
import org.apache.commons.lang.StringUtils
import org.apache.commons.lang.WordUtils
import org.springframework.stereotype.Component
import java.util.regex.Pattern

private val AFFINITY_REGEX = Pattern.compile("^io\\.rancher\\.scheduler\\.affinity:(?<type>host_label|container_label|container)(?<negate>_ne)?$")

@Component
class ProcessadorPod(
        val configuracao: Configuracao,
        val processadorVolumes: ProcessadorVolumes
) {
    val logger = logger()

    fun buildPodTemplate(service: Docker16Service, serviceName: String, rancherVolumes: Map<String, Docker16Volume>, serviceMetadata: V1ObjectMeta): V1PodTemplateSpec? {
        val affinity: V1Affinity? = buildAffinity(service.labels)
        val environment = service.environment

        val envVars = if (environment != null) {
            if (environment is Map<*, *>) {
                environment.map { V1EnvVarBuilder().withName(it.key.toString()).withValue(it.value.toString()).build()!! }
            } else if (environment is List<*>) {
                environment
                        .map {
                            val (key, value) = (it as String).split('=', limit = 2)
                            V1EnvVarBuilder().withName(key).withValue(value).build()!!
                        }
            } else {
                logger.error("Classe inválida na lista de env vars: {}", environment::javaClass.name)
                null
            }
        } else {
            null
        }

        val securityContext = if (!service.cap_add.isNullOrEmpty()) {
            V1SecurityContextBuilder()
                    .withNewCapabilities().withAdd(service.cap_add).endCapabilities()
                    .build()
        } else {
            null
        }

        val imagePullSecrets = if (configuracao.registry != null) {
            V1LocalObjectReferenceBuilder().withName(configuracao.registry).build()
        } else {
            null
        }

        val (volumeMounts, volumes) = processadorVolumes.construirVolumes(service, serviceName, rancherVolumes)

        return V1PodTemplateSpecBuilder()
                .withNewMetadata().withLabels(serviceMetadata.labels).endMetadata()
                .withNewSpec()
                .withAffinity(affinity)
                .withHostNetwork(if ("host" == service.network_mode) true else null)
                .addNewContainer()
                .withEnv(envVars)
                .withImage(service.image)
                .withImagePullPolicy(WordUtils.capitalize(StringUtils.trimToNull(service.labels["io.rancher.container.pull_image"])))
                .withName(if (service.container_name.isNullOrBlank()) serviceMetadata.name else (service.container_name!!.trim()))
                .withSecurityContext(securityContext)
                .withStdin(service.stdin_open)
                .withTty(service.tty)
                .withVolumeMounts(volumeMounts)
                .endContainer()
                .withRestartPolicy(StringUtils.capitalize(service.restart))
                .withImagePullSecrets(imagePullSecrets)
                .withVolumes(volumes)
                .endSpec()
                .build()
    }


    private fun buildAffinity(serviceLabels: Map<String, String>): V1Affinity? {
        val nodeSelectors = arrayListOf<V1NodeSelectorRequirement>()

        serviceLabels
                .map { AFFINITY_REGEX.matcher(it.key) to it.value.split(',').map { it.trim() } }
                .filter { it.first.matches() }
                .forEach {
                    val matchType = it.first.group("type")!!
                    val negate = !it.first.group("negate").isNullOrEmpty()

                    if ("host_label" == matchType) {
                        it.second.forEach {
                            val labelValor = it.split('=', limit = 2)
                            nodeSelectors.add(V1NodeSelectorRequirementBuilder()
                                    .withKey(labelValor[0])
                                    .withValues(labelValor[1])
                                    .withOperator(if (negate) "NotIn" else "In")
                                    .build()
                            )
                        }
                    } else {
                        logger.error("Match_type {} ainda não implementado", matchType)
                    }
                }

        if (nodeSelectors.isNotEmpty())
            return V1AffinityBuilder()
                    .withNewNodeAffinity()
                    .withNewRequiredDuringSchedulingIgnoredDuringExecution()
                    .addNewNodeSelectorTerm().withMatchExpressions(nodeSelectors).endNodeSelectorTerm()
                    .endRequiredDuringSchedulingIgnoredDuringExecution()
                    .endNodeAffinity()
                    .build()
        return null
    }
}
