package com.bitbucket.rpdmiranda.utils.rancher1to2

import com.bitbucket.rpdmiranda.utils.rancher1to2.rancher16.Docker16Service
import com.bitbucket.rpdmiranda.utils.rancher1to2.rancher16.Docker16Volume
import io.kubernetes.client.models.V1Volume
import io.kubernetes.client.models.V1VolumeBuilder
import io.kubernetes.client.models.V1VolumeMount
import io.kubernetes.client.models.V1VolumeMountBuilder
import org.springframework.stereotype.Component
import java.util.regex.Pattern

@Component
class ProcessadorVolumes {

    val logger = logger()

    private val PATHNAME_REGEX = Pattern.compile("^(?<root>/?).+/(?<path>[^/]*)/?$")

    fun construirVolumes(service: Docker16Service, serviceName: String, rancherVolumes: Map<String, Docker16Volume>): Pair<List<V1VolumeMount>?, List<V1Volume>?> {
        val volumeMounts: MutableList<V1VolumeMount> = arrayListOf()
        val volumes: MutableList<V1Volume> = arrayListOf()

        if (!service.volumes.isNullOrEmpty()) {
            service.volumes.forEach {
                val (hostPath, containerPath, options) = it
                        .split(':', limit = 3)
                        .map(String::trim)
                        .padRight(null, 3)

                val matcher = PATHNAME_REGEX.matcher(hostPath!!)
                if (!matcher.matches()) {
                    logger.error("Entrada de volume incorreta: {}", it)
                    return@forEach
                }
                logger.info(hostPath)
                val hostMount = ("" != matcher.group("root"))
                val nomeBase = if (!hostMount) hostPath!! else matcher.group("path")!!

                val nome: String
                if (hostMount) {
                    // Encontra o primeiro nome disponível
                    nome = if (volumes.any { it.name == nomeBase }) {
                        var sequencia = 1
                        while (volumes.any { it.name == "${nomeBase}-${sequencia}" }) {
                            sequencia++
                        }
                        "${nomeBase}-${sequencia}"
                    } else {
                        nomeBase
                    }
                    volumes.add(V1VolumeBuilder().withName(nome).withNewHostPath().withPath(hostPath).endHostPath().build())
                } else {
                    nome = hostPath!!
                    val rancherVolume = rancherVolumes[nome]
                    if (rancherVolume == null) {
                        logger.error("Volume {} não cadastrado", nome)
                    } else if ("tmpfs" != rancherVolume.driver_opts["type"]) {
                        logger.error("Volume {} possui tipo nãos suportado")
                    } else {
                        volumes.add(V1VolumeBuilder().withName(nome).withNewEmptyDir().withMedium("Memory").endEmptyDir().build())
                    }
                }
                volumeMounts.add(V1VolumeMountBuilder().withMountPath(containerPath).withName(nome).withNewReadOnly("ro" == options).build())
            }
        }
        return Pair(if (volumeMounts.isNotEmpty()) volumeMounts else null,
                if (volumes.isNotEmpty()) volumes else null)
    }
}

private inline fun <E> List<E>.padRight(e: E, size: Int): MutableList<E> {
    val list: MutableList<E> = if (this is MutableList) this else toMutableList()
    while (list.size < size)
        list.add(e)
    return list
}
