package com.bitbucket.rpdmiranda.utils.rancher1to2.rancher16

data class RancherCompose(
        var version: String? = null,
        var services: Map<String, Rancher16Service> = HashMap()
)

data class Rancher16Service(
        var start_on_create: Boolean = false,
        var scale: Int? = null,
        var health_check: Rancher16HealthCheck? = null,
        var upgrade_strategy: Rancher16UpgradeStrategy? = null
)

data class Rancher16HealthCheck(
        var port: Int? = null,
        var interval: Int? = null,
        var initializing_timeout: Int? = null,
        var reinitializing_timeout: Int? = null,
        var response_timeout: Int? = null,
        var unhealthy_threshold: Int? = null,
        var healthy_threshold: Int? = null,
        var strategy: String? = null,
        var request_line: String? = null
)

data class Rancher16UpgradeStrategy(
        var start_first: Boolean? = null,
        var interval_millis: Int? = null
)
